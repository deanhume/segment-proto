var Analytics = require('analytics-node');
var analytics = new Analytics('E9ut6w3ED6ijFalunv1iBXE6fzwYtf2v');

// Identify the user we created
analytics.identify({
  userId: 'anonymous',
  traits: {
    name: 'Harry Potter',
    email: 'potter@freshdesk.com',
    plan: 'Enterprise',
    department: 'Pothead',
    created_at: new Date('2016-07-10T00:00:00.000Z')
  },
  context: {
    timezone: 'Europe/Amsterdam',
    locale: 'en-US'
  }
});
